<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Pre-load font files for better performance (crossorigin is needed for
         font files, even if they're on the same domain). -->
    <link rel="preload" as="font" type="font/woff2" crossorigin
        href="/wp-content/themes/sam/fonts/sam-display-ultralight.woff2" />
    <link rel="preload" as="font" type="font/woff2" crossorigin
        href="/wp-content/themes/sam/fonts/sam-display-extrabold.woff2" />
    <link rel="preload" as="font" type="font/woff2" crossorigin
        href="/wp-content/themes/sam/fonts/icons/fa-solid-900.woff2" />
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    
    <!-- Load main CSS -->
    <?php wp_head(); ?>
    
    <!-- Fix for bug in Themify Product Filter plugin -->
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css?ver=1.3.1" />
    
    <!-- Google Tag Manager (Google Analytics)-->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NZ57XGM');</script>
    <!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>
    
    <a href="#content" class="screen-reader-text">Skip to content</a>
    <a href="#navigation" class="screen-reader-text">Skip to navigation</a>
    
    <?php
    
    $notice = get_theme_mod('site_notice_message', '');
    
    if ($notice) {
    ?>
    <div class="site-notice">
        <?php echo $notice; ?>
    </div>
    <?php
    }
    ?>
    
    <header class="site-header" role="banner">
        <div class="limit-width">
            
            <h1 class="site-title header-heading header-logo">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"
                class="header-logo-link">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sam-logo-black.svg"
                        alt="Shepparton Art Museum" class="header-logo-image" />
                </a>
            </h1>
            
            <nav id="navigation" class="main-navigation" role="navigation"
                aria-label="Navigation" tabindex="-1">
                
                <div class="desktop-navigation">
                    <?php wp_nav_menu(['depth' => 2]); ?>
                </div>
                
                <div class="mobile-navigation">
                    <?php wp_nav_menu(['depth' => 2]); ?>
                    
                    <div class="mobile-navigation-secondary">
                        <a href="/translate"
                           class="mobile-navigation-secondary-item">
                            <i class="mobile-navigation-secondary-icon fas fa-language"></i>
                            <span class="mobile-navigation-secondary-text">
                                Translate this page
                            </span>
                        </a>
                    </div>
                    
                </div>
            </nav>
            
            <?php
            
            $total_items_in_cart = WC()->cart->get_cart_contents_count();
            $show_cart_total = $total_items_in_cart > 0;
            
            ?>
            
            <div class="desktop-header-buttons">
                <a href="<?php echo get_page_link(get_page_by_title('Cart')->ID );?>"
                   class="header-button header-button-cart">
                    <i class="fas fa-shopping-cart" title="Cart"></i>
                    <?php if ($show_cart_total) { ?>
                        <span class="cart-total-badge">
                        <?php echo $total_items_in_cart; ?>
                        </span>
                    <?php } ?>
                </a>
                <button class="header-button header-button-search"
                    aria-expanded="false">
                    <i class="fas fa-search" title="Search"></i>
                </button>
            </div>
            
            <div class="mobile-nav-bar">
                <button
                   class="mobile-nav-bar-button mobile-nav-bar-button-menu"
                   aria-expanded="false">
                    <i class="mobile-nav-bar-button-icon fas fa-bars"></i>
                    <span class="mobile-nav-bar-button-text">Menu</span>
                </button>
                <a href="<?php echo get_page_link(get_page_by_title('Cart')->ID );?>"
                   class="mobile-nav-bar-button mobile-nav-bar-button-cart">
                    <i class="mobile-nav-bar-button-icon fas fa-shopping-cart">
                    </i>
                    <?php if ($show_cart_total) { ?>
                        <span class="cart-total-badge">
                        <?php echo $total_items_in_cart; ?>
                        </span>
                    <?php } ?>
                    <span class="mobile-nav-bar-button-text">Cart</span>
                </a>
                <button
                   class="mobile-nav-bar-button mobile-nav-bar-button-search"
                   aria-expanded="false">
                    <i class="mobile-nav-bar-button-icon fas fa-search"></i>
                    <span class="mobile-nav-bar-button-text">Search</span>
                </button>
            </div>
            
            <div class="search-panel mobile-search">
                <div class="widget widget_search">
                    <form role="search" method="get" class="search-form" action="/">
                        <label>
                            <span class="screen-reader-text">Search for:</span>
                            <input type="search" class="search-field" placeholder="Search here…" value="" name="s">
                        </label>
                        <button type="submit" class="search-submit">
                            <i class="fas fa-search"></i>
                            <span class="screen-reader-text">Search</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </header><!-- #masthead -->
    
    <div class="search-panel desktop-search">
        <div class="limit-width">
            <div class="widget widget_search">
                <form role="search" method="get" class="search-form" action="/">
                    <label>
                        <span class="screen-reader-text">Search for:</span>
                        <input type="search" class="search-field" placeholder="Search here…" value="" name="s">
                    </label>
                    <button type="submit" class="search-submit">
                        <i class="fas fa-search"></i>
                        <span class="screen-reader-text">Search</span>
                    </button>
                </form>
            </div>
        </div>
    </div>
    
    <div id="content" class="site-content" tabindex="-1">
        