(function() {
    
    // First, some polyfills.
    
    // Polyfill for NodeList.forEach()
    if (window.NodeList && !NodeList.prototype.forEach) {
        NodeList.prototype.forEach = function (callback, thisArg) {
            thisArg = thisArg || window;
            for (var i = 0; i < this.length; i++) {
                callback.call(thisArg, this[i], i, this);
            }
        };
    }
    
    // Polyfill for DOMTokenList.forEach()
    if (window.DOMTokenList && !DOMTokenList.prototype.forEach) {
        DOMTokenList.prototype.forEach = function (callback, thisArg) {
            thisArg = thisArg || window;
            for (var i = 0; i < this.length; i++) {
                callback.call(thisArg, this[i], i, this);
            }
        };
    }
    
    //Polyfill for Element.closest()
    if (!Element.prototype.matches) {
        Element.prototype.matches = Element.prototype.msMatchesSelector || 
                                    Element.prototype.webkitMatchesSelector;
    }
    if (!Element.prototype.closest) {
        Element.prototype.closest = function(s) {
            var el = this;
            if (!document.documentElement.contains(el)) return null;
            do {
                if (el.matches(s)) return el;
                el = el.parentElement || el.parentNode;
            } while (el !== null && el.nodeType === 1); 
            return null;
        };
    }
    
    // -------------------------------------------------------------------------
    
    // Wait for DOM to be ready.
    document.addEventListener('DOMContentLoaded', function() {
        /*
        
        Nav Menu Drop-downs
        -------------------

        Find menu items that have children.

        When clicked, make the link show/hide its submenu, by toggling some
        classes and aria attributes, instead of its default behaviour.
        
        The arrow icon is added with CSS.
        
        */
        var menuItems = document.querySelectorAll(
           '.desktop-navigation .menu-item-has-children, ' +
           '.mobile-navigation .menu-item-has-children');
        
        var currentlyOpenMenuItemLink = null;
        
        menuItems.forEach(function(element, key) {
            var linkElement = element.querySelector('a');
            if (linkElement === null) { return; }
            
            var subMenuElement = element.querySelector('.sub-menu');
            if (subMenuElement === null) { return; }
            
            // Give the button and the sub-menu unique ids.
            if (!linkElement.id) {
                linkElement.id = 'navigation-menu-item-' + key;
            }
            if (!subMenuElement.id) {
                subMenuElement.id = 'navigation-sub-menu-' + key;
            }
            
            linkElement.setAttribute('role', 'button');
            linkElement.setAttribute('aria-expanded', 'false');
            linkElement.setAttribute('aria-controls', subMenuElement.id);
            subMenuElement.setAttribute('aria-hidden', 'true');
            subMenuElement.setAttribute('aria-labelledby', linkElement.id);
            
            linkElement.addEventListener('click', function(event) {
                // Prevent the default link behaviour.
                event.preventDefault();
                
                // Add class to this link, the .menu-item and the .sub-menu
                var TOGGLED_CLASS = 'toggled-on';
                
                var toggleLink = event.target;
                var menuItemElement = toggleLink.closest('.menu-item');
                var subMenuElement = null;
                
                var wasExpanded = toggleLink.classList.contains(TOGGLED_CLASS);
                var isExpanded = !wasExpanded;
                
                toggleLink.classList.toggle(TOGGLED_CLASS);
                toggleLink.setAttribute(
                    'aria-expanded', isExpanded ? 'true' : 'false' );
                
                if (menuItemElement instanceof HTMLElement) {
                    subMenuElement = menuItemElement.querySelector('.sub-menu');
                    menuItemElement.classList.toggle(TOGGLED_CLASS);
                }
                if (subMenuElement instanceof HTMLElement) {
                    subMenuElement.classList.toggle(TOGGLED_CLASS);
                    subMenuElement.setAttribute(
                        'aria-hidden', isExpanded ? 'false' : 'true');
                }
                
                if (isExpanded) {
                    // If there was another menu item already open, close it.
                    if (currentlyOpenMenuItemLink instanceof HTMLAnchorElement){
                        currentlyOpenMenuItemLink.click();
                    }
                    // Set to this link.
                    currentlyOpenMenuItemLink = event.target;
                } else {
                    // We're closing this menu - clear this.
                    currentlyOpenMenuItemLink = null;
                }
                
            });
        });
        
        // Close any open menus if user clicks outside the nav.
        document.addEventListener('click', function(event) {
            var wasClickInsideNavigation = false;
            menuItems.forEach(function(element) {
                if (element.contains(event.target)) {
                    wasClickInsideNavigation = true;
                }
            });
            
            if (!wasClickInsideNavigation &&
                currentlyOpenMenuItemLink instanceof HTMLAnchorElement) {
                currentlyOpenMenuItemLink.click();
            }
        });
        
        /*
        
        Mobile - Set up buttons to toggle showing navigation and search
        ---------------------------------------------------------------
        
        Find mobile nav bar buttons and mobile nav / search elements
        
        Attach click events
        
        On click,
            - Show/hide relevant content
            - Put a class on the body to disable scrolling
            - Toggle the label on the button to say 'Close'
        
        */
        var mobileMenuButton =
            document.querySelector('.mobile-nav-bar-button-menu');
        var mobileMenuButtonIcon =
            mobileMenuButton.querySelector('.mobile-nav-bar-button-icon');
        var mobileMenuButtonText =
            mobileMenuButton.querySelector('.mobile-nav-bar-button-text');
        
        var mobileSearchButton =
            document.querySelector('.mobile-nav-bar-button-search');
        var mobileSearchButtonIcon =
            mobileSearchButton.querySelector('.mobile-nav-bar-button-icon');
        var mobileSearchButtonText =
            mobileSearchButton.querySelector('.mobile-nav-bar-button-text');
        
        var body = document.querySelector('body');
        var header = document.querySelector('.site-header');
        var mobileNavPanel = document.querySelector('.mobile-navigation');
        var mobileSearchPanel = document.querySelector('.mobile-search');
        
        /** @var currentPanel 'nav' | 'search' | null */
        var currentVisiblePanel = null;
        
        var MODAL_OPEN_CLASS = 'modal-is-open';
        var BUTTON_ACTIVE_CLASS = 'button-active';
        var BUTTON_CLOSE_ICON_CLASS = 'fa-times';
        var BUTTON_CLOSE_TEXT = 'Close';
        
        var menuButtonOriginalText = '';
        var menuButtonOriginalIconClass = '';
        var searchButtonOriginalText = '';
        var searchButtonOriginalIconClass = '';
        
        function getOriginalButtonStates() {
            menuButtonOriginalText = mobileMenuButtonText.innerHTML;
            searchButtonOriginalText = mobileSearchButtonText.innerHTML;
            
            mobileMenuButtonIcon.classList.forEach(function(className) {
                if (className.substring(0, 3) === 'fa-') {
                    menuButtonOriginalIconClass = className;
                }
            });
            mobileSearchButtonIcon.classList.forEach(function(className) {
                if (className.substring(0, 3) === 'fa-') {
                    searchButtonOriginalIconClass = className;
                }
            });
        }
        
        function updateDOMAfterButtonPress() {
            // Reset everything to the default state
            body.classList.remove(MODAL_OPEN_CLASS);
            header.classList.remove(MODAL_OPEN_CLASS);
            
            mobileNavPanel.classList.remove(MODAL_OPEN_CLASS);
            mobileSearchPanel.classList.remove(MODAL_OPEN_CLASS);
            
            mobileMenuButton.setAttribute('aria-expanded', 'false');
            mobileSearchButton.setAttribute('aria-expanded', 'false');
            mobileMenuButton.classList.remove(BUTTON_ACTIVE_CLASS);
            mobileSearchButton.classList.remove(BUTTON_ACTIVE_CLASS);
            mobileMenuButtonText.innerHTML = menuButtonOriginalText;
            mobileSearchButtonText.innerHTML = searchButtonOriginalText;
            mobileMenuButtonIcon.classList.remove(BUTTON_CLOSE_ICON_CLASS);
            mobileSearchButtonIcon.classList.remove(BUTTON_CLOSE_ICON_CLASS);
            mobileMenuButtonIcon.classList.add(menuButtonOriginalIconClass);
            mobileSearchButtonIcon.classList.add(searchButtonOriginalIconClass);
            
            if (currentVisiblePanel === null) { return; }
            
            body.classList.add(MODAL_OPEN_CLASS);
            header.classList.add(MODAL_OPEN_CLASS);
            
            if (currentVisiblePanel === 'nav') {
                mobileNavPanel.classList.add(MODAL_OPEN_CLASS);
                mobileNavPanel.focus();
                mobileMenuButton.setAttribute('aria-expanded', 'true');
                mobileMenuButton.classList.add(BUTTON_ACTIVE_CLASS);
                mobileMenuButtonText.innerHTML = BUTTON_CLOSE_TEXT;
                mobileMenuButtonIcon.classList
                    .remove(menuButtonOriginalIconClass);
                mobileMenuButtonIcon.classList.add(BUTTON_CLOSE_ICON_CLASS);
            }
            
            if (currentVisiblePanel === 'search') {
                mobileSearchPanel.classList.add(MODAL_OPEN_CLASS);
                mobileSearchButton.setAttribute('aria-expanded', 'true');
                mobileSearchButton.classList.add(BUTTON_ACTIVE_CLASS);
                mobileSearchButtonText.innerHTML = BUTTON_CLOSE_TEXT;
                mobileSearchButtonIcon.classList
                    .remove(searchButtonOriginalIconClass);
                mobileSearchButtonIcon.classList.add(BUTTON_CLOSE_ICON_CLASS);
           }
            
        }
        
        getOriginalButtonStates();
        
        mobileMenuButton.addEventListener('click', function(event) {
            event.preventDefault();
            if (currentVisiblePanel === 'nav') {
                currentVisiblePanel = null;
            } else {
                currentVisiblePanel = 'nav';
            }
            updateDOMAfterButtonPress();
        });
        
        mobileSearchButton.addEventListener('click', function(event) {
            event.preventDefault();
            if (currentVisiblePanel === 'search') {
                currentVisiblePanel = null;
            } else {
                currentVisiblePanel = 'search';
            }
            updateDOMAfterButtonPress();
        });
        
        
        
        /*
        
        Desktop - make search button show/hide the search box
        
        */
        var desktopSearchButton =
            document.querySelector('.header-button-search');
        var desktopSearchButtonIcon =
            document.querySelector('.header-button-search i');
        var desktopSearchPanel = document.querySelector('.desktop-search');
        
        if (desktopSearchButton && desktopSearchPanel) {
            desktopSearchButton.addEventListener('click', function(event) {
                event.preventDefault();
                var isOpening = !desktopSearchPanel.classList.contains(
                    'desktop-search-visible');
                
                if (isOpening) {
                    desktopSearchButton.setAttribute('aria-expanded', 'true');
                } else {
                    desktopSearchButton.setAttribute('aria-expanded', 'false');
                }
                
                desktopSearchPanel.classList.toggle('desktop-search-visible');
                desktopSearchButtonIcon.classList.toggle('fa-search');
                desktopSearchButtonIcon.classList.toggle('fa-times');
            });
        }
    });
})();