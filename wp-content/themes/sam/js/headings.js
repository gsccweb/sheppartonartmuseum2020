(function() {
    
    // Polyfill for NodeList.forEach()
    if (window.NodeList && !NodeList.prototype.forEach) {
        NodeList.prototype.forEach = function (callback, thisArg) {
            thisArg = thisArg || window;
            for (var i = 0; i < this.length; i++) {
                callback.call(thisArg, this[i], i, this);
            }
        };
    }
    
    
    
    
    // Adds a dot to the end of certain headings, to match the style guide.
    // Doing this in JS since we need to check if the headings already have a
    // dot, or other punctuation.
    
    var headings = document.querySelectorAll(
        '.content-heading, ' +
        'h2:not(.content-subheading)' + /* No whitespace between each :not() */
          ':not(.tribe-events-schedule__datetime)' +
          ':not(.woocommerce-loop-product__title)' +
          ':not(.woocommerce-loop-category__title)' +
          ':not(.product_title), ' +
        'h6,' +
        '.promotion-title');
    
    var punctuation = ['.', '?', '!', ':', ';'];
    
    headings.forEach(function(element) {
        
        var lastCharacter = element.textContent.trim().slice(-1);
        
        if (punctuation.indexOf(lastCharacter) === -1) {
            element.textContent = element.textContent.trim() + '.';
        }
        
    });
    
})();