(function() {
    
    // Removes WooCommerce CSS from DOM - we can't do this with the
    // dequeue_style PHP function because there is a plugin (Themify Product
    // Filter) that manually adds it.
    document.addEventListener('DOMContentLoaded', function() {
        var elementIds = [
            'woocommerce-general-css',
            'woocommerce-layout-css',
            'woocommerce-smallscreen-css'
        ];
        
        for (var i = 0; i < elementIds.length; i++) {
            var element = document.getElementById(elementIds[i]);
            if (element instanceof HTMLElement) {
                element.parentNode.removeChild(element);
            }
        }
    });
    
})();