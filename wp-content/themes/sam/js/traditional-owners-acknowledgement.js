(function() {
    // Shows a modal popup the first time you visit the website.
    
    var modal = document.querySelector('.traditional-owners-acknowledgement');
    var dismissButton = modal
        .querySelector('.traditional-owners-acknowledgement-dismiss-button');
    
    if (! (modal instanceof Element) || ! (dismissButton instanceof Element)) {
        return;
    }
    
    var localStorage = window.localStorage;
    var STORAGE_KEY = 'sam-traditional-owners-acknowledgement-seen-datetime';
    var SHOW_EVERY_N_WEEKS = 2;
    
    function numberOfWeeksBetween(date2, date1) 
    {
        var difference =(date2.getTime() - date1.getTime()) / 1000;
        difference /= (60 * 60 * 24 * 7);
        return Math.abs(Math.round(difference));
    }
    
    var now = new Date();
    var previouslySeenDate = null;
    var userHasSeenRecently = false;
    var alwaysShow = false;
    
    if (localStorage.getItem(STORAGE_KEY) !== null) {
        previouslySeenDate = new Date(localStorage.getItem(STORAGE_KEY));
    }
    
    if (previouslySeenDate !== null && // Make sure date is valid
        !isNaN(previouslySeenDate) &&
        numberOfWeeksBetween(now, previouslySeenDate) <= SHOW_EVERY_N_WEEKS) {
        userHasSeenRecently = true;
    } else {
        userHasSeenRecently = false;
    }
    
    if (document.location.hash === '#traditional-owners') {
        alwaysShow = true;
    }
    
    // Don't show if the user has recently seen
    if (userHasSeenRecently && alwaysShow === false) { return; }
    
    // Show modal
    modal.style.display = '';
    dismissButton.addEventListener('click', function() {
        modal.style.display = 'none';
        
        // Make sure website is at the top when modal disappears (in case the
        // user's scrolling has affected the overall scroll position).
        window.scrollTo(0, 0);
        
        // Save in local storage so we know not to show the message again.
        var currentDate = new Date();
        localStorage.setItem(STORAGE_KEY, currentDate.toUTCString());
    });
})();