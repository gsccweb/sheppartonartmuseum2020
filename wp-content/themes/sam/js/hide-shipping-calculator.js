(function() {
    
    // Polyfill for NodeList.forEach()
    if (window.NodeList && !NodeList.prototype.forEach) {
        NodeList.prototype.forEach = function (callback, thisArg) {
            thisArg = thisArg || window;
            for (var i = 0; i < this.length; i++) {
                callback.call(thisArg, this[i], i, this);
            }
        };
    }
    
    var shippingMethodRadioButtonsSelector = 'input[name^="shipping_method"]';
    var hideShippingCalculatorClass = 'hide-shipping-calculator';
    
    var showOrHideShippingCalculator = function(action) {
        if (action === 'show') {
            document.body.classList.remove(hideShippingCalculatorClass);
        } else {
            document.body.classList.add(hideShippingCalculatorClass);
        }
    };
    
    var onSelectShippingMethodRadioButton = function(element) {
        if (element.getAttribute('type') === 'radio' && !element.checked) {
            return;
        }
        
        var isLocalPickupSelected = element.value.match(/^local_pickup/);
        
        showOrHideShippingCalculator(isLocalPickupSelected ? 'hide' : 'show');
    };
    
    // Set the correct show/hide state on page load.
    window.addEventListener('load', function(event) {
        var shippingMethodRadioButtons =
            document.querySelectorAll(shippingMethodRadioButtonsSelector);
        shippingMethodRadioButtons.forEach(function(element) {
            onSelectShippingMethodRadioButton(element);
        })
    });
    
    // Show or hide whenever the user changes the shipping method (ajax).
    document.addEventListener('change', function(event) {
        // This listener catches ANY change event in the document. We're only
        // interested in events triggered by our shipping method radio buttons.
        // We need to attach the event to the document rather than directly to
        // the radio buttons, because the radio buttons get replaced whenever
        // the shipping method changes.
        var shippingMethodRadioButtons =
            document.querySelectorAll(shippingMethodRadioButtonsSelector);
        shippingMethodRadioButtons.forEach(function(element) {
            if (event.target === element) {
                onSelectShippingMethodRadioButton(element);
            }
        })
    });
})();