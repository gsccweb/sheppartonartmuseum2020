<?php get_header(); ?>
    <main class="site-main" role="main">
        
        <ul class="promotions">
            <?php
            
            $promotions = CFS()->get('promotions');
            
            foreach($promotions as $promotion) {
                // Skip unpublished promotions.
                if (!$promotion['published']) { continue; }
            ?>
            <li class="promotion"
                style="background-color: <?php echo htmlentities($promotion['background_colour']); ?>">
                <a class="promotion-link" href="<?php echo htmlentities($promotion['url']); ?>"
                   style="color: <?php echo htmlentities($promotion['text_colour']); ?>">
                    <div class="promotion-image"
                        style="background-image:
                            url('<?php echo wp_get_attachment_image_url($promotion['image'], 'main-image'); ?>')">
                    </div>
                    <div class="promotion-details">
                        <div class="promotion-details-inner">
                            <p class="promotion-title">
                                <?php echo htmlentities($promotion['title']); ?>
                            </p>
                            <p class="promotion-sub-title">
                                <?php echo htmlentities($promotion['subtitle']); ?>
                            </p>
                            <div class="promotion-description">
                                <?php echo $promotion['fine_print']; ?>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            <?php } ?>
        </ul>
        
        <div class="events events-home">
            <div class="limit-width-wide limit-width-no-padding">
                <h2>What's On</h2>
                
                <div class="event-items">
                <?php echo do_shortcode('[tribe_events_list limit="6"]'); ?>
                </div>
                
                <a class="button" href="/events">
                    See more events
                </a>
            </div>
        </div>
        
        <?php
        
        // Apparently SAM don't want news on the home page now, so I've
        // set it to display: none below.
        
        $query = new WP_Query([
            'post_type' => 'post',
            'posts_per_page' => 6
        ]);
        
        if ($query->have_posts()) {
        ?>
            <div class="latest-news" style="display: none">
                <div class="limit-width-wide limit-width-no-padding">
                    <h2>Latest news</h2>
                    <ul class="news-items">
                    <?php
                    
                    while($query->have_posts()) {
                        $query->the_post();
                    ?>
                        
                        <li class="news-item">
                            <a class="news-item-link"
                            href="#<?php //the_permalink(); ?>">
                                <div class="news-item-image"
                                    <?php if (has_post_thumbnail()) { ?>
                                    style="background-image: url('<?php the_post_thumbnail_url('large'); ?>')"
                                    <?php } else { ?>
                                    data-use-fallback
                                    <?php } ?>
                                ></div>
                                
                                <div class="news-item-details">
                                    <span class="news-item-title">
                                        <?php the_title(); ?>
                                    </span>
                                    <span class="news-item-date">
                                        <?php the_date(); ?>
                                    </span>
                                </div>
                            </a>
                        </li>
                        
                    <?php
                    }
                    wp_reset_postdata();
                    ?>
                    </ul>
                    
                    <!-- <a class="button has-white-background-color" href="/news"> -->
                    <a class="button has-white-background-color" href="#">
                        See more news
                    </a>
                    
                </div>
            </div>
        <?php } ?>
        
        <div class="home-sections">
            
            <div class="follow-us home-section">
                <div class="limit-width">
                    <?php echo CFS()->get('social_media'); ?>
                    <ul class="social-logos">
                        <li class="social-logo">
                            <a href="http://www.facebook.com/pages/Shepparton-Art-Museum/192801704088348"
                            class="social-logo-link" title="Follow us on Facebook">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook-logo-black.svg"
                                    alt="Facebook" class="social-logo-image" />
                            </a>
                        </li>
                        <li class="social-logo">
                            <a href="http://instagram.com/SAM_Shepparton" class="social-logo-link" title="Instagram">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/instagram-logo-black.svg"
                                    alt="Instagram" class="social-logo-image" />
                            </a>
                        </li>
                        <li class="social-logo">
                            <a href="https://www.pinterest.com/sheppartonhouse/shepparton-art-museum/"
                            class="social-logo-link" title="Pinterest">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pinterest-logo-black.svg"
                                    alt="Pinterest" class="social-logo-image" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            
            
            <div class="become-a-member home-section">
                <div class="limit-width">
                    <?php echo CFS()->get('membership'); ?>
                    <p>
                    <?php
                        $membership_button = CFS()->get('membership_button');
                        $membership_url = $membership_button['url'];
                        $membership_text = $membership_button['text'];
                        ?>
                        <a href="<?php echo $membership_url; ?>" class="button">
                            <?php echo $membership_text; ?>
                        </a>
                    </p>
                </div>
            </div>
            
            
            <div class="subscribe home-section">
                <div class="limit-width">
                    <?php echo CFS()->get('subscribe'); ?>
                    <p>
                        <?php
                        $subscribe_button = CFS()->get('subscribe_button');
                        $subscribe_url = $subscribe_button['url'];
                        $subscribe_text = $subscribe_button['text'];
                        ?>
                        <a href="<?php echo $subscribe_url; ?>" class="button">
                            <?php echo $subscribe_text; ?>
                        </a>
                    </p>
                </div>
            </div>
            
        </div>
        
        <?php
        $logos_file_id = CFS()->get('logos');
        $logos_file_url = wp_get_attachment_image_url($logos_file_id, 'full');
        $logos_alt_text = get_post_meta(
            $logos_file_id, '_wp_attachment_image_alt');
        
        if ($logos_file_url) {
        ?>
        <div class="home-page-logos">
            <div class="limit-width">
                <img src="<?php echo $logos_file_url; ?>"
                     alt="<?php echo $logos_alt_text[0]; ?>" />
            </div>
        </div>
        <?php
        }
        ?>
    </main>
<?php get_footer(); ?>