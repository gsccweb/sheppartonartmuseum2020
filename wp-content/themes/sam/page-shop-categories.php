<?php get_header(); ?>
    <?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
    ?>
        <main class="site-main shop-categories-page" role="main">
            
            <?php
            
            // Content editors can set these colours when editing the page,
            // (uses the Custom Field Suite plugin).
            $heading_background_colour = 'black';
            $heading_text_colour = 'white';
            
            if (CFS()->get('heading_background')) {
                $heading_background_colour = CFS()->get('heading_background');
            }
            if (CFS()->get('heading_text')) {
                $heading_text_colour = CFS()->get('heading_text');
            }
            
            ?>
            <header class="content-header"
                style="background-color:
                    <?php echo htmlentities($heading_background_colour); ?>;
                    color:
                    <?php echo htmlentities($heading_text_colour); ?>">
                <div class="limit-width">
                    <h1 class="content-heading">Shop</h1>
                    <?php
                    
                    $subheading = CFS()->get('subheading');
                    
                    if ($subheading) {
                        echo '<h2 class="content-subheading">'.
                                $subheading.
                                '</h2>';
                    }
                    
                    ?>
                </div>
            </header>
            
            <div class="content-body-outer">
                <div class="limit-width">
                    <div class="content-body">
                        <div class="shop-categories">
                        <?php
                        
                        $categories = get_categories([
                            'taxonomy'     => 'product_cat',
                            'orderby'      => 'name',
                            
                            // 1 for yes, 0 for no
                            'show_count'   => 0,
                            'pad_counts'   => 0,
                            'hierarchical' => 0,
                            'hide_empty'   => 1,
                            
                            'fields'       => 'all',
                            'title_li'     => ''
                        ]);
                        
                        // print_r($categories);
                        
                        foreach ($categories as $category) {
                            if ($category->name === 'Uncategorised') {
                                continue; // Exclude the Uncategorised category.
                            }
                            ?>
                            <a href="/shop/?wpf=1&id=shop_filters&wpf_wpf_cat=<?php echo htmlentities($category->slug); ?>"
                                class="shop-category shop-category-link">
                                <?php
                                $thumbnail_id = get_term_meta(
                                    $category->term_id, 'thumbnail_id', true);
                                $thumbnail_url = wp_get_attachment_image_url(
                                    $thumbnail_id, 'large');
                                
                                if ($thumbnail_url === false) {
                                    $thumbnail_url = wc_placeholder_img_src('large');
                                }
                                ?>
                                <span class="shop-category-image-wrapper">
                                    <img class="shop-category-image"
                                        src="<?php echo $thumbnail_url; ?>" />
                                </span>
                                <span class="shop-category-name">
                                    <?php echo $category->name; ?>
                                </span>
                            </a>
                            <?php
                        }
                        ?>
                        </div>
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </main>
    <?php
        }
    }
    ?>
<?php get_footer(); ?>