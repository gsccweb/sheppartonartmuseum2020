    </div><!-- #content -->
    
    <footer class="site-footer" role="contentinfo">
        <div class="limit-width">
            
            <div class="site-footer-sections">
                <div class="site-footer-section site-footer-section-info">
                    <div class="footer-address-and-contact">
                    <p>
                        <a href="/" class="footer-business-name"><strong>Shepparton Art Museum</strong></a><br />
                        <?php
                        // Address
                        $address = get_theme_mod('footer_address', '');
                        echo nl2br($address);
                        ?>
                    </p>
                    <?php
                    // Contact Details
                    $contact = get_theme_mod('footer_contact', '');
                    
                    if ($contact) {
                        echo '<p>'.$contact.'</p>';
                    }
                    ?>
                    </div>
                    
                    <?php
                    // Open Hours
                    $open_hours = get_theme_mod('footer_open_hours', '');
                    
                    if ($open_hours) {
                        echo '<p>'.nl2br($open_hours).'</p>';
                    }
                    ?>
                </div>
                
                <div class="site-footer-section site-footer-section-links">
                    <?php
                    // Links
                    $links = get_theme_mod('footer_links', '');
                    
                    if ($links) {
                        echo '<p class="footer-links">'.$links.'</p>';
                    }
                    ?>
                    <ul class="social-logos">
                        <li class="social-logo">
                            <a href="http://www.facebook.com/pages/Shepparton-Art-Museum/192801704088348"
                            class="social-logo-link" title="Follow us on Facebook">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook-logo-black.svg"
                                    alt="Facebook" class="social-logo-image" />
                            </a>
                        </li>
                        <li class="social-logo">
                            <a href="http://instagram.com/SAM_Shepparton" class="social-logo-link" title="Instagram">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/instagram-logo-black.svg"
                                    alt="Instagram" class="social-logo-image" />
                            </a>
                        </li>
                        <li class="social-logo">
                            <a href="https://www.pinterest.com/sheppartonhouse/shepparton-art-museum/"
                            class="social-logo-link" title="Pinterest">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pinterest-logo-black.svg"
                                    alt="Pinterest" class="social-logo-image" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            
        </div><!-- .limit-width -->
    </footer>
    
    <div class="traditional-owners-acknowledgement" style="display: none">
        <div class="traditional-owners-acknowledgement-inner">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sam-logo-black.svg"
                alt="Shepparton Art Museum"
                class="traditional-owners-acknowledgement-logo" />
            <?php
            $traditional_owners_content =
                get_theme_mod('traditional_owners_content', '');
            echo $traditional_owners_content;
            ?>
            <p>
                <button class="button
                               traditional-owners-acknowledgement-dismiss-button">
                    Proceed
                </button>
            </p>
        </div>
    </div>

<?php wp_footer(); ?>

</body>
</html>