<?php get_header(); ?>
<main class="site-main" role="main">
    
    <header class="content-header">
        <div class="limit-width">
            <h1 class="content-heading">
                Search Results
            </h1>
            <h2 class="content-subheading content-subheading-search-query">
                “<?php the_search_query(); ?>”
            </h2>
        </div>
    </header>
    
    <div class="content-body-outer">
        <div class="limit-width">
            <div class="content-body">
                    
                    <?php
                    if (have_posts()) {
                        while (have_posts()) {
                            the_post();
                            ?>
                            <a class="search-result search-result-link"
                               href="<?php the_permalink(); ?>">
                                
                                <div class="search-result-image"
                                    <?php if (has_post_thumbnail()) { ?>
                                    style="background-image: url('<?php the_post_thumbnail_url('large'); ?>')"
                                    <?php } else { ?>
                                    data-use-fallback
                                    <?php } ?>
                                ></div>
                                
                                <div class="search-result-content">
                                    <h3 class="search-result-title">
                                        <?php the_title(); ?>
                                    </h3>
                                    
                                    <div class="search-result-excerpt">
                                        <?php the_excerpt(); ?>
                                    </div>
                                    
                                    <?php
                                    if (get_post_type() === 'tribe_events') {
                                        ?>
                                        <div class="search-result-date">
                                            <?php 
                                                $start = tribe_get_start_date(
                                                    $post, false, 'j F Y');
                                                
                                                $end = tribe_get_end_date(
                                                    $post, false, 'j F Y');
                                                
                                                echo $start;
                                                if ($end !== $start) {
                                                    echo ' - '.$end;
                                                }
                                            ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    
                                    <?php
                                    if (get_post_type() === 'post') {
                                        ?>
                                        <div class="search-result-date">
                                            <?php the_date(); ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    
                                    <?php
                                    $typeName = '';
                                    switch(get_post_type()) {
                                        case 'page':
                                            $typeName = 'Page';
                                            break;
                                        case 'post':
                                            $typeName = 'News Article';
                                            break;
                                        case 'tribe_events':
                                            $typeName = 'Event';
                                            break;
                                        case 'product':
                                            $typeName = 'Shop';
                                            break;
                                    };
                                    
                                    if ($typeName) {
                                        ?>
                                        <p class="search-result-type">
                                            <?php echo $typeName; ?>
                                        </p>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </a>
                            <?php
                        }
                    } else {
                    ?>
                        <p>Nothing found.</p>
                        <p>Try going to the <a href="<?php echo home_url(); ?>">
                        home page</a>.</p>
                    <?php } ?>
                    
                </div>
            </div>
        </div>
</main>
                
<?php get_footer(); ?>