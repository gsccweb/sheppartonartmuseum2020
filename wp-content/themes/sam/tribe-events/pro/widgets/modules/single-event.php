<?php
/**
 * Single Event Template for Widgets
 *
 * This template is used to render single events for both the calendar and advanced
 * list widgets, facilitating a common appearance for each as standard.
 *
 * You can override this template in your own theme by creating a file at
 * [your-theme]/tribe-events/pro/widgets/modules/single-event.php
 *
 * @version 5.0.0
 *
 * @package TribeEventsCalendarPro
 */

$post_date = tribe_events_get_widget_event_post_date();
$post_id   = get_the_ID();

?>

<!-- <div class="event-item"> -->
	<a class="event-item-link" href="<?php echo tribe_get_event_link(); ?>">
		<div class="event-item-image"
			<?php if (get_post_thumbnail_id($post_id)) { ?>
				style="background-image: url('<?php the_post_thumbnail_url('large'); ?>')"
			<?php } else { ?>
			data-use-fallback
			<?php } ?>
		></div>
		
		<div class="event-item-details">
			<span class="event-item-title">
				<?php the_title(); ?>
			</span>
			<span class="event-item-date">
				<?php echo tribe_events_event_schedule_details(); ?>
			</span>
		</div>
	</a>
<!-- </div> -->