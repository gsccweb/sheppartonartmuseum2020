<?php

add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles', 999);
function my_theme_enqueue_styles() {
    
    $sam_theme_version = wp_get_theme()->get('Version');
    
    wp_enqueue_style('sam',
        get_stylesheet_directory_uri() . '/style.css',
        array(),
        $sam_theme_version
    );
    wp_enqueue_style('sam-icons',
        get_stylesheet_directory_uri() . '/icons.css',
        array(),
        $sam_theme_version
    );
    wp_enqueue_style('sam-fonts-nunito',
        'https://fonts.googleapis.com/css?family=Nunito+Sans:400,400i,700&display=swap');
    
    wp_enqueue_script('sam-navigation',
        get_stylesheet_directory_uri() . '/js/navigation.js',
        array(),
        $sam_theme_version,
        true
    );
    
    wp_enqueue_script('sam-headings',
        get_stylesheet_directory_uri() . '/js/headings.js',
        array(),
        $sam_theme_version,
        true
    );
    
    // Hides the shipping calculator on the Cart page if Local Pickup is
    // selected
    wp_enqueue_script('sam-hide-shipping-calculator',
        get_stylesheet_directory_uri() . '/js/hide-shipping-calculator.js',
        array(),
        $sam_theme_version,
        true
    );
    
    // Removes WooCommerce CSS from DOM - we can't do this with the
    // dequeue_style function because there is a plugin (Themify Product Filter)
    // that manually adds it.
    wp_enqueue_script('sam-remove-woocommerce-css',
        get_stylesheet_directory_uri() . '/js/remove-woocommerce-css.js',
        array(),
        $sam_theme_version,
        true
    );
    
    // Shows the Traditional Owners Acknowledgement modal popup when you first
    // visit the website.
    wp_enqueue_script('sam-traditional-owners-acknowledgement',
        get_stylesheet_directory_uri() .
            '/js/traditional-owners-acknowledgement.js',
        array(),
        $sam_theme_version,
        true
    );
    
    // Remove The Events Calendar styles.
    wp_dequeue_style('tribe-common-skeleton-style');
    wp_dequeue_style('tribe-events-views-v2-skeleton');
    wp_dequeue_style('tribe-filterbar-mobile-styles');
    wp_dequeue_style('tribe-filterbar-styles');
    wp_dequeue_style('tribe-common-skeleton-style');
    wp_dequeue_style('tribe-events-views-v2-skeleton');
    wp_dequeue_style('tribe-events-views-v2-bootstrap-datepicker-styles');
    wp_dequeue_style('tribe-tooltip');
    wp_dequeue_style('tribe-events-filterbar-views-filter-bar-styles');
    wp_dequeue_style('tribe-events-filterbar-views-v2-1-filter-bar-skeleton');
    wp_dequeue_style('tribe-events-pro-views-v2-skeleton');
    wp_dequeue_style('tribe-events-calendar-pro-style');
    wp_dequeue_style('tribe-events-calendar-full-pro-mobile-style');
    wp_dequeue_style('tribe-events-calendar-pro-mobile-style');
    
    // Can't seem to disable many of these because there are JavaScript files
    // that list these as dependencies, so they get loaded regardless.
    // wp_dequeue_style('event-tickets-tickets-css');
    // wp_dequeue_style('event-tickets-plus-tickets-css');
    // wp_dequeue_style('event-tickets-tickets-rsvp-css');
    // wp_dequeue_style('event-tickets-rsvp');
    // wp_dequeue_style('event-tickets-tpp-css');
    // wp_dequeue_style('event-tickets-registration-page-styles');
    
    // Remove The Events Calendar Filter Bar javascript?
    // wp_dequeue_script('tribe-events-filterbar-views-filter-bar-js');
    
    // Remove WooCommerce styles if necessary.
    wp_dequeue_style('woocommerce-general');
    wp_dequeue_style('woocommerce-layout');
    wp_dequeue_style('woocommerce-smallscreen');
}

add_action('after_setup_theme', 'my_theme_setup');
function my_theme_setup() {    
    add_theme_support('title-tag');
    
    add_theme_support('post-thumbnails');
    add_image_size('main-image', 2000, 2000); // 2000px x 2000px

    // Colour options in Wordpress 'Gutenberg' editor.

    // Disable custom colour selection, so user must pick a brand colour.
    // add_theme_support( 'disable-custom-colors' );
    
	// Editor Color Palette
	add_theme_support('editor-color-palette', [
        ['name' => 'Black', 'slug' => 'black', 'color' => '#000'],
        ['name' => 'White', 'slug' => 'white', 'color' => '#fff'],
        ['name' => 'Yellow', 'slug' => 'yellow', 'color' => '#FFC20E'],
        ['name' => 'Orange', 'slug' => 'orange', 'color' => '#F47920'],
        ['name' => 'Red', 'slug' => 'red', 'color' => '#EF4030'],
        ['name' => 'Pink', 'slug' => 'pink', 'color' => '#E74098'],
        ['name' => 'Purple', 'slug' => 'purple', 'color' => '#812990'],
        ['name' => 'Dark-Blue', 'slug' => 'dark-blue', 'color' => '#27557D'],
        ['name' => 'Light-Blue', 'slug' => 'light-blue', 'color' => '#00B9E8'],
        ['name' => 'Teal', 'slug' => 'teal', 'color' => '#89CCCA'],
        ['name' => 'Light-Green', 'slug' => 'light-green', 'color' => '#98CB4F'],
        ['name' => 'Dark-Green', 'slug' => 'dark-green', 'color' => '#41BA7C'],
    ]);
}

// Set image compression level (default is 82). We're using fairly large
// image dimensions to cater for high-dpi screens, so the quality can actually
// be turned down a bit without being that noticable.
add_filter('jpeg_quality', function() { return 65; });

/* Add support for uploading JFIF image files. */
add_filter('upload_mimes', function($mime_types) {
    $mime_types['jfif'] = 'image/jfif+xml';
    return $mime_types;
});

// Customise date/time output for event lists.
add_filter('tribe_events_event_schedule_details_formatting', function($settings) {
    $settings['time'] = false;
    return $settings;
});

// Change the default fields when adding a calendar event.
add_filter('tribe_events_editor_default_template', function( $template ) {
	$template = [
        [ 'tribe/event-datetime' ],
        // [ 'tribe/featured-image'],
		[ 'core/paragraph', [ 
			'placeholder' => __( 'Add Description...', 'the-events-calendar' ), 
        ] ],
        [ 'tribe/event-price' ],
        // [ 'tribe/event-organizer' ],
        [ 'tribe/field-ecpcustom2' ], // "Audience" custom field
        [ 'core/paragraph', [ 
			'placeholder' => __( 'Add Content...', 'the-events-calendar' ), 
        ] ],
        [ 'tribe/event-venue' ],
        // [ 'tribe/event-website' ],
        // [ 'tribe/event-links' ]
	];
	return $template;
}, 11, 1);



/*
 * THEME SETTINGS
 * 
 * This defines some settings in the Theme Editor / Customize user interface
 * where the user can change things like the "pick-up only" text that shows up
 * on the product page, cart, and checkout.
 * 
 * Then there are some hooks to display those "pick-up only" messages in the
 * relevant places.
 */
add_action( 'customize_register', function( WP_Customize_Manager $wp_customize ) {
    $wp_customize->add_section( 'sam-section' , array(
        'title'      => __( 'SAM Theme', 'sam' ),
        'priority'   => 1,
    ) );
    
    // Site Notice (banner text at top of every page)
    $wp_customize->add_setting( 'site_notice_message' , array(
        'default'   => '',
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control(
        'site_notice_message',
        [
            'type' => 'text',
            'setting' => 'site_notice_message',
            'label' => 'Site Notice',
            'section' => 'sam-section'
        ]
    );
    
    // Footer - Address
    $wp_customize->add_setting( 'footer_address' , array(
        'default'   =>
            "70 Welsford Street, Shepparton\r\n".
            "Victoria, Australia",
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control(
        'footer_address',
        [
            'type' => 'textarea',
            'setting' => 'footer_address',
            'label' => 'Footer - Address',
            'section' => 'sam-section'
        ]
    );
    // Footer - Contact Details
    $wp_customize->add_setting( 'footer_contact' , array(
        'default'   =>
'<strong>p</strong> 
<a href="tel:+61348045000">+61 (03) 4804 5000</a><br />

<strong>e</strong> 
<a href="mailto:info@sheppartonartmuseum.com.au">
    info@sheppartonartmuseum.com.au
</a>',
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control(
        'footer_contact',
        [
            'type' => 'textarea',
            'setting' => 'footer_contact',
            'label' => 'Footer - Contact Details',
            'section' => 'sam-section'
        ]
    );
    // Footer - Open Hours
    $wp_customize->add_setting( 'footer_open_hours' , array(
        'default'   =>
            "<b>FREE ENTRY</b>\r\n".
            "Open 7 days, 10am to 4pm\r\n".
            "Public holidays 1pm to 4pm\r\n".
            "SAM is open every day except Christmas Day, New Year’s Day ".
            "and Good Friday.",
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control(
        'footer_open_hours',
        [
            'type' => 'textarea',
            'setting' => 'footer_open_hours',
            'label' => 'Footer - Open Hours',
            'section' => 'sam-section'
        ]
    );
    // Footer - Links
    $wp_customize->add_setting( 'footer_links' , array(
        'default'   =>
'<a href="/contact/">Contact us</a><br />
<a href="/membership/">Become a member</a><br />
<a href="/news-and-updates/">Subscribe to email updates</a><br />
<br />
<a href="/privacy/">Privacy</a><br />
<a href="/copyright-and-reproduction/">Copyright</a><br />
<a href="/disclaimer/">Disclaimer</a><br />
<a href="/translate/">Translate</a>',
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control(
        'footer_links',
        [
            'type' => 'textarea',
            'setting' => 'footer_links',
            'label' => 'Footer - Links',
            'section' => 'sam-section'
        ]
    );
    
    // Traditional Owners Acknowledgement
    $wp_customize->add_setting( 'traditional_owners_content' , array(
        'default'   =>
'<h2>Traditional Owners Acknowledgement.</h2>
<p>
Shepparton Art Museum acknowledges the Traditional Owners of the
Land of Greater Shepparton: the Yorta Yorta Nation, whose clans
include the Bangerang, Kailtheban, Wollithiga, Moira, Ulupna,
Kwat Kwat, Yalaba Yalaba and Ngurai-illiam-wurrung people.
</p>
<p>
We pay respect to their Tribal Elders, we celebrate their
continuing culture, and we acknowledge the memory of their
ancestors.
</p>',
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control(
        'traditional_owners_content',
        [
            'type' => 'textarea',
            'setting' => 'traditional_owners_content',
            'label' => 'Traditional Owners Acknowledgement',
            'description' => '<a href="/#traditional-owners" target="_blank">'.
                             'Click here to preview</a>',
            'section' => 'sam-section'
        ]
    );
    
    // Order received page.
    $wp_customize->add_setting( 'order_received_page_message' , array(
        'default'   => 'Thank you. Your order has been received. If you purchased any products marked as “pick-up only”, we’ll email you when they’re ready to be picked up.',
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control(
        'order_received_page_message',
        [
            'type' => 'text',
            'setting' => 'order_received_page_message',
            'label' => 'Order Received Page - Message',
            'section' => 'sam-section'
        ]
    );
} );





/*
 * WOOCOMMERCE
 */

// Make certain product categories 'click and collect' only (no shipping).
add_filter('woocommerce_package_rates', function($rates, $package) {
    
    // HERE set your product categories in the array (IDs, slugs or names)
    $categories = ['ceramics'];
    
    // AFTER YOU CHANGE THE CATEGORIES, you will need to refresh the shipping
    // caches by going into the WooCommerce Shipping settings, and disabling
    // one of the shipping methods, clicking Save, then re-enabling it and
    // clicking Save again.
    
    $found = false;
    // Loop through each cart item, checking for the defined product categories.
    foreach($package['contents'] as $cart_item) {
        if (has_term($categories, 'product_cat', $cart_item['product_id'])) {
            $found = true;
            break;
        }
    }
    
    $rates_arr = [];
    if ($found) {
        foreach($rates as $rate_id => $rate) { 
            if ('local_pickup' === $rate->method_id) {
                $rates_arr[$rate_id] = $rate;
            }
        }
    }
    
    return !empty($rates_arr) ? $rates_arr : $rates;
    
}, 90, 2 );



// Output title on single product page, just after the image gallery.
add_action('woocommerce_single_product_summary', function($product) {
    the_title('<h2 class="product_title entry-title">', '</h2>');
}, 5);


// Customise the "Place Order" button on the Checkout page.
add_filter('woocommerce_order_button_text', function() {
    return __('Pay for order', 'woocommerce');
});


// Order Received page.
add_filter('woocommerce_thankyou_order_received_text', function($text, $order) {
    return get_theme_mod('order_received_page_message');
}, 10, 2);

/* Cart - modify markup slightly so that Cart Totals section can have a
   full-width background colour */
add_action('woocommerce_before_cart_collaterals', function() {
    // Need to close 3 <div>s to close the .limit-width container.
            echo '</div>';
        echo '</div>';
    echo '</div>';
    
    echo '<div class="cart-totals-section">'; // We close this below.
        echo '<div class="limit-width">';
            echo '<div class="content-body">';
                echo '<div class="woocommerce">';
});
add_action('woocommerce_after_cart', function() {
    echo '</div>'; // Closes our .cart-totals-section above.
});

/**
 * Trim zeros in price decimals
 */
add_filter( 'woocommerce_price_trim_zeros', '__return_true' );









// Event Tickets Pro - remove notice on Wordpress Dashboard about geolocation for your venues.
add_action( 'admin_notices', function() {
    if ( ! class_exists( 'Tribe__Events__Pro__Geo_Loc' ) ) { return; }
    
    $geoloc = Tribe__Events__Pro__Geo_Loc::instance();
    
    remove_action( 'admin_notices', array(
        $geoloc,
        'show_offer_to_fix_notice'
    ) );
}, 1 );









// Search Results - prevent hidden products (including event tickets) from
// appearing in public search results.
add_action('pre_get_posts', function($query = false) {
    if(!is_admin() && is_search() && is_object($query)) {
        $query->set( 'tax_query', [
            'relation' => 'OR',
            [
              'taxonomy' => 'product_visibility',
              'field'    => 'name',
              'terms'    => 'exclude-from-catalog',
              'operator' => 'NOT IN',
            ],
            [
              'taxonomy' => 'product_visibility',
              'field'    => 'name',
              'terms'    => 'exclude-from-catalog',
              'operator' => '!=',
            ],
        ]);
    }
});