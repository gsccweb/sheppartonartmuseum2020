<?php

$is_events_page = is_post_type_archive(Tribe__Events__Main::POSTTYPE);
$is_event_page = tribe_is_event() && is_single();
$is_products_page = is_shop() || is_product_category() || is_product_tag();
$is_product_page = function_exists('is_product') ? is_product() : false;

// For some reason, on single event pages, the $post global is not available
// while in the loop below, so we have to fetch these details here.
$event_title = $is_event_page ? $post->post_title : '';
$event_image = $is_event_page ? get_the_post_thumbnail(null, 'main-image') : '';

$has_main_image = (has_post_thumbnail() || $event_image)
                  && !$is_product_page
                  && !$is_events_page;

?>
<?php get_header(); ?>
    <?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
    ?>
        <main class="site-main
                    <?php if($is_events_page) { ?>
                        whats-on
                    <?php } ?>
                    <?php if ($has_main_image) { ?>
                        has-main-image
                    <?php } ?>
                    "
              role="main">
                
                <?php if ($has_main_image) { ?>
                <div class="content-main-image">
                    <?php if ($is_event_page) { ?>
                        <?php echo $event_image; ?>
                    <?php } else { ?>
                        <?php the_post_thumbnail('main-image'); ?>
                    <?php } ?>
                </div>
                <?php } ?>
                
                <?php
                
                // Content editors can set these colours when editing the page,
                // (uses the Custom Field Suite plugin).
                $heading_background_colour = 'black';
                $heading_text_colour = 'white';
                
                if (CFS()->get('heading_background')) {
                    $heading_background_colour = CFS()->get('heading_background');
                }
                if (CFS()->get('heading_text')) {
                    $heading_text_colour = CFS()->get('heading_text');
                }
                
                ?>
                <header class="content-header"
                    style="background-color:
                        <?php echo htmlentities($heading_background_colour); ?>;
                        color:
                        <?php echo htmlentities($heading_text_colour); ?>">
                    <div class="limit-width">
                        <h1 class="content-heading">
                            <?php if($is_events_page) { ?>
                                What's On
                            <?php } else if ($is_event_page) { ?>
                                <?php echo $event_title ?>
                            <?php } else if ($is_product_page) { ?>
                                Shop
                            <?php } else { ?>
                                <?php the_title(); ?>
                            <?php } ?>
                        </h1>
                        <?php
                        
                        $subheading = CFS()->get('subheading');
                        
                        if ($subheading) {
                            echo '<h2 class="content-subheading">'.
                                 $subheading.
                                 '</h2>';
                        }
                        
                        ?>
                    </div>
                </header>
                
                <div class="
                        content-body-outer
                        <?php echo $is_products_page ? ' shop-page' : '' ?>
                        ">
                    <div class="limit-width">
                        <div class="content-body">
                            <?php
                            
                            if ($is_products_page) {
                                echo do_shortcode(
                                    '[searchandfilter id="shop_filters"]');
                            }
                            ?>
                            <?php
                            
                            // Show date on news articles.
                            if (get_post_type() === 'post') {
                                echo '<p class="post-date">'.
                                     get_the_date().
                                     '</p>';
                            }
                            
                            ?>
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
                
                <?php
                
                // On event pages, show the 'Subscribe to email updates' section
                // at the bottom.
                if($is_events_page || $is_event_page) {
                
                ?>
                <div class="footer-subscribe">
                    <div class="limit-width">
                        <?php
                            echo CFS()->get(
                                'subscribe', /* From Home page (ID: 2) */2);
                        ?>
                        <p>
                            <?php
                            // Same content as the subscribe section on the home
                            // page
                            $subscribe_button = CFS()->get(
                                'subscribe_button',
                                /* From Home page (ID: 2) */2);
                            $subscribe_url = $subscribe_button['url'];
                            $subscribe_text = $subscribe_button['text'];
                            ?>
                            <a href="<?php echo $subscribe_url; ?>"
                               class="button">
                                <?php echo $subscribe_text; ?>
                            </a>
                        </p>
                    </div>
                </div>
                <?php } ?>
        </main>
    <?php
        }
    } else {
    ?>
        <main class="site-main" role="main">
            <header class="content-header">
                <div class="limit-width">
                    <h2 class="content-heading">Content not found</h2>
                </div>
            </header>
            <div class="content-body-outer">
                <div class="limit-width">
                    <div class="content-body">
                        <p>Try going to the <a href="<?php echo home_url(); ?>">
                        home page</a>, or use the search function.</p>
                    </div>
                </div>
            </div>
        </main>
    <?php } ?>
<?php get_footer(); ?>