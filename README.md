# Shepparton Art Museum website

This repo basically only contains the theme files for the SAM website.

To use this for local development you'll need to install WordPress and any of
the relevant plugins that are being used on the live site. You'll probably also
need to copy the database from the live site to your local site, using something
like WP Migrate DB, so that you'll have the same settings and website content.

Instructions for WP Migrate DB:

Assuming you’re using the free version, simply install the plugin. Go to Tools >
Migrate DB. Fill in the URL(s) you’d like found and what to replace them with.
To find what the folder path is on the live server, log into WordPress on the
live server and go to Tools > Migrate DB, and you'll see the correct values
pre-populated in the fields. Copy these to the Migrate DB page on your local
site.

Export your WordPress database, which will download an SQL file. Now access your
MySQL (or MariaDB!) database on the live server using your preferred tool (like
phpMyAdmin), import the SQL file, and you’re good to go.